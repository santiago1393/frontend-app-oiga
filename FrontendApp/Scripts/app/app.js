﻿var app = angular.module('myApp', ['ngResource', 'ngRoute']);

var validarPass = function (pass) {    
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    if (strongRegex.test(pass)) {
        return true;
    }
    else {
        return false;   
    }
};

app.constant('ENDPOINT_URL', { url: 'http://localhost:56933/api/api/Usuarios/' });



app.config(['$routeProvider', function config($routeProvider) {  
    $routeProvider.when('/', {
        controller: 'listController',
        templateUrl: '/app/lista.cshtml'
    }).
    when('/edit', {
        controller: 'editController',
        templateUrl: '/app/edit.cshtml'
    }).
    when('/delete/:id', {
        controller: 'deleteController',
        templateUrl: '/app/delete.cshtml'
    }).
    when('/new', {
        controller: 'newController',
        templateUrl: '/app/new.cshtml'
    }).
    otherwise(
    {
        redirectTo: '/'
            });  
}]);

app.controller('deleteController', ['$scope', 'ENDPOINT_URL', '$http', '$routeParams', '$location', function ($scope,  ENDPOINT_URL, $http, $routeParams, $location) {
    var userid = $routeParams["id"];
    console.log(userid);
    $scope.dataok = false;
    $scope.confirmar = function () {
        $http.delete(ENDPOINT_URL.url, { params: { id: userid } }).then(function (data) {
            $scope.dataok = true;
            alert("Usuario borrado con exito!");
            $location.path('/list');  
        }, function (data, status) {
            $scope.mss = "Error al conectar la BD";
            $scope.dataok = false;
        });
    }

    
}]);


app.controller('listController', ['$scope', '$rootScope', 'ENDPOINT_URL', '$http', '$location', function ($scope, $rootScope, ENDPOINT_URL, $http, $location) {
    $scope.hello = 'Bienvenidos';
    $scope.url = ENDPOINT_URL.url;   
    $scope.dataok = false;
    $scope.mss = "";
    $http.get(ENDPOINT_URL.url).then(function (data) {        
        $scope.Empleados = data.data;
        $scope.dataok = true;
    }, function (data, status) {
        $scope.mss = "Error al conectar la BD";        
        $scope.dataok = false;
    });

    $scope.nuevo = function () {
        $location.path('/new');
    }
    
    $scope.borrar = function (id) {
        $location.path('/delete/' + id);      
    }

    $scope.editar = function (usuario) {        
        $location.path('/edit/').search({ param: usuario });    
    };

}]); 


app.controller('editController', ['$scope', '$rootScope', 'ENDPOINT_URL', '$http', '$routeParams', '$location', function ($scope, $rootScope, ENDPOINT_URL, $http, $routeParams, $location) {

    if ($routeParams["param"] === undefined) {
        $location.path('/list');
    }

    $scope.usuario = $routeParams["param"];
    $http.get(ENDPOINT_URL.url, { params: { id: $scope.usuario } }).then(function (data) {
        $scope.userdata = data.data;
        $scope.dataok = true;
    }, function (data, status) {
        $scope.mss = "Error al conectar la BD";
        $scope.dataok = false;
        });

    $scope.update = function () {
        var okpass = validarPass($scope.userdata.Password);
        if ($scope.userdata.Name !== undefined) {
            if (okpass) {
                $scope.passok = true;

                $http.put(ENDPOINT_URL.url + "?id=" + $scope.usuario + "&name=" + $scope.userdata.Name + "&pass=" + $scope.userdata.Password).
                    then(function (data) {
                        alert("Usuario Actualizado con exito!");
                        $location.path('/list');
                    }, function (data, status) {
                        alert(data);
                        $scope.mss = "Error al conectar la BD";
                        $scope.dataok = false;
                    });
            } else {
                $scope.passok = false;
                $scope.mss = "Error en la contraseña, minimo 8 caracteres, con 1 digito y un caracter especial!";
            }
        } else {
            $scope.passok = false;
            $scope.mss = "Nombre Requerido";
        }

    };

    $scope.volver = function () {
        $location.path('/list');
    }

}]);


app.controller('newController', ['$scope', '$rootScope', 'ENDPOINT_URL', '$http', '$routeParams', '$location', '$httpParamSerializerJQLike', function ($scope, $rootScope, ENDPOINT_URL, $http, $routeParams, $location, $httpParamSerializerJQLike) {
   
  
    $scope.crear = function () {
        var okpass = validarPass($scope.password);
        if ($scope.name !== undefined) {
            if (okpass) {
                $scope.passok = true;
                
                $http.post(ENDPOINT_URL.url + "?name=" + $scope.name + "&pass=" + $scope.password).
                then(function (data) {
                    alert("Usuario Creado con exito!");
                    $location.path('/list');  
                    }, function (data, status) {                        
                        $scope.mss = "Error al conectar la BD";
                        $scope.passok = false;
                });
            } else {
                $scope.passok = false;
                $scope.mss = "Error en la contraseña, minimo 8 caracteres, con 1 digito y un caracter especial!";
            }
        } else {
            $scope.passok = false;
            $scope.mss = "Nombre Requerido";
        }

    };

    $scope.volver = function () {
        $location.path('/list');
    }

}]);

app.controller('timer', ['$scope', '$timeout', function ($scope, $timeout) {
    $scope.contador = 0;
    $scope.onTimeout = function () {
        $scope.contador++;
        mytimeout = $timeout($scope.onTimeout, 1000);
    }
    var mytimeout = $timeout($scope.onTimeout, 1000);
}]);



